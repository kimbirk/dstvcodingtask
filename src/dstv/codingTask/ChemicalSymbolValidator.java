package dstv.codingTask;
/**
 * 
 * @author Kim
 * date: 18/06/2017
 *
 */
public class ChemicalSymbolValidator implements IValidator{
	
	public ChemicalSymbolValidator(){		
	}

	public boolean isValid( String name, String symbol ){

		if( symbol == null )
			return false;
		// check for condition 1. All chemical symbols must be exactly two letters
		if( symbol.length() != 2 )
			return false;

		symbol = symbol.toLowerCase();
		name = name.toLowerCase();
		
		// check for condition 2. Both letters in the symbol must appear in the element name		
		char first = symbol.charAt(0);
		char last = symbol.charAt(1);
		CharSequence cs1 = first + "";
		CharSequence cs2 = last + "";
		if( !( name.contains(cs1) && name.contains(cs2 ) )){
			return false;
		}
		
		// check for condition 3. The two letters must appear in order in the element name.		
		int index = name.indexOf( first );		
		String substring = name.substring(index+1);
		if( !substring.contains( cs2 ) ){
			return false;
		}		
		// the check for condition 3 above also satisfies condition 4:If the two letters in the symbol are the same, 
		// it must appear twice in the element name.
		
		return true;
	}
}
