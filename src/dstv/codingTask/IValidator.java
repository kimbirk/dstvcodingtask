package dstv.codingTask;
/**
 * 
 * @author Kim
 *
 */
public interface IValidator {

	public boolean isValid( String name, String symbol );
}
