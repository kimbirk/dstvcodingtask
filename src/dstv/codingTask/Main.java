package dstv.codingTask;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Kim
 * date: 18/06/2017
 * 
 * given two strings, one an element name and one a proposed symbol for that element,
 * this program will output true or false depending on
 * whether the proposed symbol meets the naming conditions
 * assume args[0] is the element name
 * and args[1] is the symbol name
 * 
 */
public class Main {

	public static void main( String[] args ){

		IValidator validator = new ChemicalSymbolValidator();
		if( args.length > 1 ){
			String name=args[0].trim();
			String symbol=args[1].trim();
			System.out.println( name + ", " + symbol + "->" + validator.isValid(name, symbol));			
		}		
		// parse and test the examples:
		List<String> exampleList = new ArrayList<String>();
		exampleList.add("Spenglerium, Ee");
		exampleList.add("Zeddemorium, Zr");
		exampleList.add("Venkmine, Kn");
		exampleList.add("Stantzon, Zt");
		exampleList.add("Melintzum, Nn");
		exampleList.add("Tullium, Ty");

		for( String example : exampleList ){
			String[] parts=example.split(",");
			String name=parts[0];
			String symbol=parts[1].trim();
			System.out.println( example + "->" + validator.isValid(name, symbol));
		}		
	}
}
